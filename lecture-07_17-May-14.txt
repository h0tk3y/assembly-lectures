﻿Режимы работы процессора

    Real mode -- режим прцоессора, который был исходно в i8086. В i80286 стало понятно, что
        нужны изменения, потому что в этом режиме "всем можно всё", и нормальная операционная
        система в таком режиме неосуществима, потому что даже ошибки в программе будут рушить
        работу ОС. Поэтому нужно по меньшей мере разграничение доступа к RAM (а на самом деле
        не только) для кода операционной системы и программ. У современных процессоров эти
        режимы реализованы как режим ядра и режим userspace-программ.
        
        |         CR0.PE = 0 ^ 
        |                    |
        v CR0.PE = 1         |
        
    Protected mode -- защищенный режим, добавленный в i286. Оба режима оставались 16-битными
        В i386 добавили 32-битный защищённый режим, и это два разных режима.
        
        |      EFLAGS.VM = 0 ^
        |                    |
        v EFLAGS.VM = 1      |
        
    V86 -- режим, также добавленный в i386.
    
    Есть четыре кольца доступа, на нулевом можно всё, на третьем -- сильно ограниченный набор,
        но в операционных системах из них используются только нулевое и третье.
        
    Работа в реальном режиме.
        
        Разграничение доступа осуществляется через сегментную модель. В реальном (16-битном)
            режиме адресация выглядит как segment:offset. Сегмент задаётся одним из сегментных
            регистров -- CS, DS, SS, ES (FS, GS добавлены позже). Полный адрес в любом случае
            всегда состоит из сегмента и смещения. Например, полный адрес следующей выполняемой
            команды -- это CS:IP, при обращении к данным сегментная часть адреса берётся из DS,
            например, при MOV AL, [BX] происходит обращение к DS:BX.
            
        В реальном режиме адрес определялся очень просто: segment*16 + offset, такая модель
            позволяла адресовать чуть больше одного мегабайта, и для 86-го процессора, у которого
            памяти было как раз 1 Мб, обращение к памяти делалось по модулю.
            У более современных процессоров, сразу после включения работающих в реальном 16-битном
            режиме, обращение к памяти было таким же, хоть его и можно было отключить в BIOS,
            позже такое поведение убрали.
            
        Адреса в такой модели адресации не уникальны, один и тот же адрес можно по-разному
            указать сегментом и смещения.
        
        При таком обращении к памяти можно указывать сегмент вручную, MOV AL, ES:[BX]. "ES:" --
            это префикс, который для следующей команды указывает сегмент. Повторять префиксы тоже
            можно, и с префиксами длина команды x86 не ограничена, хоть при этом действует
            соглашение: максимальная длина команды -- 15 байт, и на более длинных командых
            генерируется ошибка. Если префиксов у команды несколько, то железом используется
            последний из них.
            
        Умолчания:
            - при обращении к программному коду (jmp, call) -- CS
            - при обращении к данным:
                если в качестве базового регистра используются BP, EPB, ESP -- SS 
                иначе -- DS                                    // ^ e.g. mov al, [bp+di+3]
                
        При обращении к [EDI + EBP + 3] сегмент однозначно не определён, потому что из
            32-битных регистров как EDI, так и EBP могут быть базовыми, и разные ассемблеры
            могут сгенерировать разный код. У YASM можно написать [EBP + EDI*1 + 3], тогда
            умноженный регистр будет индексным. Но с MASM такое уже не работает из-за оптимизации
            умножения на единицу.
            
        В командах, обращение к памяти в которых не содержит явного адреса (например, MOVSB),
            сегменты полного адреса тоже указаны в спецификации, например, MOVSB: DS:SI --> ES:DI.
            
        Команды, обращающиеся к стеку, используют в качестве сегментного регистра SS.\
        
        ES, FS и GS -- сегментные регистры, не имеющие специального назначения.
    
        С сегментной адресацией связано разграничение на COM, EXE и т.д. В COM все сегменты
            программы начинаются в нуле, код идёт в начале сегмента, а стек -- в конце.
            
    Защищённый режим
    
        Защита реализована в изменении сегментной модели.
        Адресация неизменна, умолчания -- тоже.
        
        Полностью изменяется интерпретация сегмента (получение из сегмента и смещения
            реального адреса).
            
        Теперь 16-битный сегментный регистр рассматривается так (по битам)
            0..1  : RPL - requested privilege level.
            2     : GDT/LDT (см. ниже)
            3..15 : индекс
            
        Эта информация указывает на 8-байтную структуру, находящуюся в таблице 
            по GDTR -- 6-байтнгого регистра (начало, размер), укзывающего на массив сегментов.
            
        При загрузке числа в сегментный регистр поле индекса -- это индекс в таблице на номер
            записи в массиве. У записи есть база -- число, которое прибавляется к смещению.
            
        Чтобы получить адрес для обращения к памяти, теперь нужно [gtdr + segment].base + offset.
        
        Кроме базы у структуры в таблице сегментов есть ещё четыре байта с дополнительной
            информацией о сегменте: 20-битное поле лимита определяет размер данного сегмента, и 
            обращение к сегментам возможно только по смещениям 0..limit, limit можно настроить либо
            в байтах, либо в (4 Кб)-страницах. Есть бит, позволяющий инвертировать лимит, позволяющий
            инвертировать лимит, чтобы он разрешал доступ по смещениям -limit..0.
            Отдельным битом определяется, является сегмент сегментом кода или сегментом данных.
            Сегмент кода всегда доступен на выполнение, доступ на чтение может быть как разрешён,
            так и запрещён, а вот запись в сегмент кода запрещена. В сегментный регистр CS можно
            загрузить только сегмент кода. В сегментный регистр SS можно загрузить только сегмент данных,
            доступный для записи.
            
        Про структуру сегментного регистра рекомендуется отдельно почитать самостоятельно.
        
        Таблиц сегментов может быть две: глобальная, на которую указывает регистр GDTR, и локальная,
            на сегмент с которой укзывает двухбайтный регистр LDTR.
            
        Современные ОС не используют локальную таблицу сегментов, то есть, в segment[2] == 0,
            а сегменты переключаются другим способом.
            
        RPL -- requested privilege level. Для регистра CS это поле называется CPL -- current.
            Этими двумя битами CPL определяется набор разрешённых действий, 00 -- нулевое кольцо,
            11 -- третье. В третьем кольце при изменении сегментных регистров проверяется, разрешено
            ли загружать указанный сегмент.
            
        Понизить CPL можно, повысить -- нет. Вернуть нулевое кольцо можно с помощью можно с помощью
            механизма прерываний, в том числе программно: команда int работает аналогично аппаратным
            прерываниям, и у процессора есть таблица с обработчиками прерываний (в реальном режиме эта
            таблица находится по адресу 0, в защищённом -- адрес указан в регистре IDTR). При прерывании
            могут переключаться права: у обработчика какого-то прерывания могут быть установлены
            привилегии нулевого кольца. Содержимое таблицы обработчиков прерываний настраивает ОС, и для
            её изменения также нужны права нулевого кольца. Механизм прерываний довольно тяжелый, что ещё
            приемлемо для аппаратных прерываний, но для системных вызовов -- уже нет, и для этих целей в
            современных процессорах есть команды (Intel) SYSENTER и SYSEXIT, и ядро можно вызывать обоими
            способами -- и прерываниями, и этими командами.
            SYSENTER -- безусловный переход в ядро, код в котором сделает SYSEXIT, но не в то же место, а
            по фиксированному адресу, который уже содержит код возврата в userspace. У AMD есть своя
            альтернатива -- syscall и sysret, что очень    похоже, но возврат делается уже по адресу, с
            которого был syscall. Intel это расширение поддержали. В 64-битном режиме используются
            syscall/sysret, потому что процессоры AMD в 64-битном режиме их не поддерживают.
            
        Кольцом доступа также определяются права на порты ввода-вывода, работа с которыми осуществляется
            командами IN и OUT. Для каждого порта хранится кольцо доступа, необходимое для работы с ним.        
            Win9x разрешали доступ в третьем кольце ко всем портам ввода-вывода, что было плохо в плане
            защиты, но хорошо для программирования железа. В WinNT для свободной программной работы с
            IOPorts нужно писать драйвер, что значительно сложнее, но закрывать IOPorts для защиты 
            совершенно необходимо, потому что свободный доступ к ним позволяет в обход ОС работать с хардом,
            видеокартой, питанием и т.д.
            
        Про адресацию в защищенном режиме можно почитать отдельно.
            
        Дескрипторами сегментов можно описывать много чего ещё, например, gate'ы вызовов.
            
        16-битный и 32-битный защищённые режимы отличаются только в умолчаниях адресации.
        
    V86 -- режим, необходимый для обеспечения работы софта, написанного для реального режима,
        в операционных системах, работающих с защищенным режимом, без потери безопасности.
        
        Этот режим с точки зрения адресации работает как реальный режим, то есть, при загрузке
            в сегментный регистр числа, это число считается действительно адресом начала сегмента,
            а права в V86 -- третье кольцо защищённого режима, и запрещённые действия обрабатываются
            операционной системой, которая в обработчике может эмулировать нужное действие, например,
            работу с видеокартой.
        
        В AMD Athlon 64 
        
    В 64-битном режиме произошли значительные изменения:
        
        Появляется long mode, состоящий из двух подрежимов, а именно Compatibility mode и 64-bit mode.
        
        Compatibility mode -- промежуточный режим, из которого можно переключаться в protected
            mode (EFER.LME = 1; CR4.PAE = 1; CR0.PG = 1) и обратно (CR0.PG = 0; EFER.LME = 0),
            //      ^ extended feature enabled register
            а также в 64-bit mode (установкой или сбросом EFER.LME)
            
        Идея -- максимальная совместимость с тем, что было раньше, и совместимость обеспечивается
            на уровне прикладных программ, не драйверов. С точки зрения программ compatibility mode
            практически не отличается от protected mode (sysenter/sysexit), а 64-bit mode уже
            предоставляет все возможности 64-битной архитектуры. 16- и 32-битные защищённые режимы
            отличались одним битом в сегментном дескрипторе. К нему добавили ещё один бит, и теперь
                00 -- 16-битный сегмент;
                01 -- 32-битный сегмент;
                10 -- 64-битный сегмент;
                11 -- зарезервировано.
            Сегментом, выбранным как CS, определяется интерпретация процессором опкодов как 16-, 32-
            или 64-битных.
        
        Современные ОС используют только малую часть аппаратных возможностей, в том числе и сегментной
            модели. Например, почти все сегменты устанавливаются с базой 0 и размером 4 Gb. Разграничение
            прав реализовано с помощью страничной адресацией (о которой в следующей лекции). AMD решили
            избавиться от сложной сегментной модели, и в их процессорах в Long mode используются 4-битные
            сегментные дескрипторы (права и битность). В сегментах данных не используется вообще никакая
            информация. Обращения к данным всегда воспринимаются как обращения к сегменту с базой 0 и
            лимитом 4 Gb. От сегментов FS и GS осталось только одно поле базы, и обращение к {FS\GS}:offset
            осмысленно. Например, в современных ОС эти сегменты используются для локальных данных потоков.
            Например, при вызовах к WinAPI можно получить код ошибки, и если обращения к WinAPI делаются в
            несколько потоков, то ещё один вызов может нарушить эту информацию. Поэтому код ошибки хранится
            локально для потока, и GetLastError делает вот что: MOV EAX, FS:[const]; RET. Для каждого потока
            FS и GS используются для создания TLS в 32-битном и 64-битном режимах.
            
        Последствия: ОС при работе в Long mode практически не может эмулировать реальный режим (переход в него
            может быть выполнен только с помощью RESET). В 64-битных Windows поэтому отключена поддержка 
            DOS-программ. А ещё из 64-bit mode в Protected mode и обратно переходить теоретически можно, но
            из-за интерпретации информации в CS для настройки прав произойдёт ошибка.
        
        ОС находится в Long mode и оперирует 64-битным адресным пространством и адреса поэтому требуются
            соответствующие, процесс их создания -- сложнее, чем просто перекомпилировать, потому что требует
            адаптации под новую адресацию. Пользовательским же приложениям почти всё равно, в каком из режимов
            (Protected / Compatibility) они работают, работа с 32-битным кодом осуществляется просто загрузкой
            сегмента с 32-битным кодом как CS. В Windows все процессы 64-битные, но 32-битные процессы загружают
            дополнительные библиотеки эмуляции 32-битного режима, которые уже осуществляют загрузку 32-битной
            программы (настройку сегментов). В результате у процесса может могут быть загружены как 32-битные,
            так и 64-битные библиотеки. Процессу достаточно знания, что он 32-битный, а ОС -- 64-битная, и
            обращения к ОС для загрузки 64-битной библиотеки не через слой эмуляции, а напрямую.
            Но работа 32-битного процесса с 64-битными библиотеками требует аккуратности.
            
    Итого: режимы процессора различаются в первую очередь адресацией к памяти:
        - реальный режим: 1 Mb и простая адресация;
        - защищённый режим: сколько угодно памяти (одновременно регистрами можно адресоваться к 4 Гб);
        - V86: 1 Mb и адресация как в реальном, но права третьего кольца защищённого режима;
        - Long mode - compatibility: упрощённая сегментная адресация защищённого режима.
        - Long mode - 64-bit: 64-битная адресация.
        
    Real mode (16-bit) -------- Protected mode (16-/32-bit) --------- V86(16-bit)
                                            |
                                            |
                            Compatibility mode (16-/32-/64-bit) ------- 64-bit mode
                            
    В пределах одного блока интерпретация команд разной битности делается загрузкой в CS
        сегмента нужной битности.
        
    Реализация контроля доступа в железе позволяет ОС обеспечить себе полную безопасность, и
        все дыры в них -- всего лишь неправильная настройка прав железа. Главное отличие
        современных процессоров общего доступа от видеокарт -- это именно система ограничения
        доступа, которая видеокартам вовсе и не нужна. У Intel в Project Knights используются
        ядра х86, и поэтому на них может работать операционная система (которая там даже есть).